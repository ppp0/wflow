package main

type VertexTimeout struct {
	New  uint32 `json:"new"`
	Wait uint32 `json:"wait"`
	Run  uint32 `json:"run"`
}

type VertexService struct {
	name    string
	timeout VertexTimeout

	graph *Graph

	childs []Vertex

	new  *TaskContainerNew
	wait *TaskContainerWait
	run  *TaskContainerRun

	cache VertexStorage
}

func NewVertexService(name string, graph *Graph, cache VertexStorage, timeout VertexTimeout) *VertexService {
	v := new(VertexService)
	v.name = name
	v.new = NewTaskContainerNew(timeout.New)
	v.wait = NewTaskContainerWait(timeout.Wait)
	v.run = NewTaskContainerRun(timeout.Run)
	v.graph = graph
	v.timeout = timeout
	v.cache = cache
	v.childs = make([]Vertex, 0)
	return v
}

func (v *VertexService) InsertTasks(tasks []VertexTask) {
	done := v.new.Insert(tasks)
	v.cache.Put(v.graph.Name(), v.name, tasks, v.timeout.New, TaskNew)

	if len(done) > 0 {
		v.wait.Insert(done)
		v.cache.Put(v.graph.Name(), v.name, done, v.timeout.Wait, TaskWait)
	}
}

func (v *VertexService) GetTasks(count uint) []VertexTask {
	tasks := v.wait.Get(count)
	v.run.Insert(tasks)
	v.cache.Put(v.graph.Name(), v.name, tasks, v.timeout.Run, TaskRun)
	return tasks
}

func (v *VertexService) CommitTasks(tasks []VertexTask) {
	v.run.Remove(tasks)
	for _, child := range v.childs {
		child.InsertTasks(tasks)
	}
	v.cache.Delete(v.graph.Name(), v.name, tasks)
}

func (v *VertexService) CollectGarbage() []VertexTask {
	var garbage []VertexTask
	v.new.CollectGarbage(&garbage)
	v.wait.CollectGarbage(&garbage)
	v.run.CollectGarbage(&garbage)
	return garbage
}

func (v *VertexService) GraphName() string {
	return v.graph.name
}

func (v *VertexService) Name() string {
	return v.name
}

func (v *VertexService) AddParent(vertex *VertexService) {
	vertex.childs = append(vertex.childs, v)
	v.new.parents++
}

func (v *VertexService) GetMaxExecutionTime() uint32 {
	return v.new.ttl + v.wait.ttl + v.run.ttl
}

func (v *VertexService) Childs() []Vertex {
	return v.childs
}

func (v *VertexService) SetNewTimeout(ttl uint32) uint32 {
	if ttl > v.new.ttl {
		v.new.ttl = ttl
	}
	return v.GetMaxExecutionTime()
}

func (v *VertexService) SetTimeout(waitTtl uint32, runTtl uint32) {
	v.wait.ttl = waitTtl
	v.run.ttl = runTtl
}
