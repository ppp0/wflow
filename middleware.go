package main

type FilterMiddleware interface {
	Filter(vertexTasks *[]VertexTask, tasks *[]Task) ([]ErrorTask, error)
}

type PayloadMiddleware interface {
	Payload(graph string, vertex string, tasks []Task) ([]TaskRequest, []ErrorTask, error)
}
