package main

import (
	"github.com/rs/xid"
	"sync"
	"time"
)

type TaskContainerRun struct {
	items map[xid.ID]VertexTaskContext
	ttl   uint32
	sync.Mutex
}

func NewTaskContainerRun(ttl uint32) *TaskContainerRun {
	c := new(TaskContainerRun)
	c.items = make(map[xid.ID]VertexTaskContext)
	c.ttl = ttl
	return c
}

func (c *TaskContainerRun) Insert(tasks []VertexTask) {

	ttl := uint32(time.Now().Unix()) + c.ttl

	c.Lock()
	defer c.Unlock()

	for i, task := range tasks {
		task.TTL = ttl
		c.items[task.ID] = task.VertexTaskContext
		tasks[i] = task
	}
}

func (c *TaskContainerRun) Remove(tasks []VertexTask) {

	c.Lock()
	defer c.Unlock()

	for _, task := range tasks {
		item, ok := c.items[task.ID]
		if ok {
			task.Priority = item.Priority
			delete(c.items, task.ID)
		}
	}
}

func (c *TaskContainerRun) CollectGarbage(garbage *[]VertexTask) {

	now := uint32(time.Now().Unix())

	c.Lock()
	defer c.Unlock()

	for id, ctx := range c.items {
		if ctx.TTL <= now {
			*garbage = append(*garbage, VertexTask{id, ctx})
			delete(c.items, id)
		}
	}
}
