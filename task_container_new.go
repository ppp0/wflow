package main

import (
	"github.com/rs/xid"
	"sync"
	"time"
)

type TaskContainerNew struct {
	items   map[xid.ID]VertexTaskContext
	ttl     uint32
	parents uint8
	sync.Mutex
}

func NewTaskContainerNew(ttl uint32) *TaskContainerNew {
	c := new(TaskContainerNew)
	c.items = make(map[xid.ID]VertexTaskContext)
	c.ttl = ttl
	return c
}

func (c *TaskContainerNew) Insert(tasks []VertexTask) []VertexTask {

	now := uint32(time.Now().Unix())

	done := make([]VertexTask, 0, len(tasks))

	c.Lock()
	defer c.Unlock()

	for i, task := range tasks {
		if c.parents == 0 || c.parents == 1 {
			done = append(done, task)
			continue
		}

		existedTask, ok := c.items[task.ID]
		if ok {
			existedTask.Parents--
			if existedTask.Parents == 0 {
				done = append(done, task)
				delete(c.items, task.ID)
			} else {
				c.items[task.ID] = existedTask
				task.Parents = existedTask.Parents
			}
		} else {
			task.TTL = now + c.ttl
			task.Parents = c.parents - 1
			c.items[task.ID] = task.VertexTaskContext
		}
		tasks[i] = task
	}

	return done
}

func (c *TaskContainerNew) CollectGarbage(garbage *[]VertexTask) {
	now := uint32(time.Now().Unix())

	c.Lock()
	defer c.Unlock()

	for id, ctx := range c.items {
		if ctx.TTL <= now {
			*garbage = append(*garbage, VertexTask{id, ctx})
			delete(c.items, id)
		}
	}
}
