package main

type Vertex interface {
	InsertTasks(tasks []VertexTask)
	GetTasks(count uint) []VertexTask
	CommitTasks(tasks []VertexTask)
	CollectGarbage() []VertexTask
	GraphName() string
	Name() string
	Childs() []Vertex
	SetNewTimeout(ttl uint32) uint32
}
