package main

import (
	"encoding/json"
	"github.com/rs/xid"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
)

type uriHandler struct {
	method  string
	uri     *regexp.Regexp
	handler func(w http.ResponseWriter, req *http.Request, items []string)
}

type HttpApi struct {
	graphs   []Graph
	handlers []uriHandler
}

func NewHttpApi(graphs []Graph) *HttpApi {
	h := new(HttpApi)
	h.graphs = graphs

	h.handlers = []uriHandler{
		{"", regexp.MustCompile(`/graphs/(\w+)/services/(\w+)`), h.getTasks},
		{"GET", regexp.MustCompile(`/graphs/(\w+)/services/(\w+)`), h.getTasks},
		{"POST", regexp.MustCompile(`/graphs/(\w+)/services/(\w+)`), h.submitResults},
		{"POST", regexp.MustCompile(`/graphs/(\w+)`), h.createTasks},
	}

	return h
}

func (h *HttpApi) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	defer func() {
		if req.Body != nil {
			if err := req.Body.Close(); err != nil {
				logger.Printf("%s\n", err)
			}
		}
	}()
	for _, handler := range h.handlers {
		if handler.method == req.Method {
			items := handler.uri.FindStringSubmatch(req.URL.Path)
			if items == nil {
				continue
			}
			handler.handler(w, req, items)
			return
		}
	}
	w.WriteHeader(http.StatusNotFound)
	if _, err := w.Write([]byte("not found")); err != nil {
		logger.Printf("%s\n", err)
	}
}

func (h *HttpApi) createTasks(w http.ResponseWriter, req *http.Request, items []string) {
	graphName := items[1]
	graph := findGraph(h.graphs, graphName)
	if graph == nil {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("not found")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	if req.Body == nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte("body is null")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	var requests []TaskRequest
	if err := json.Unmarshal(data, &requests); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	createdTasks := graph.CreateTasks(requests)
	tasks := make([]xid.ID, 0, len(createdTasks))
	for _, task := range createdTasks {
		tasks = append(tasks, task.ID)
	}
	data, err = json.Marshal(tasks)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(data); err != nil {
		logger.Printf("%s\n", err)
	}
}

func (h *HttpApi) getTasks(w http.ResponseWriter, req *http.Request, items []string) {
	graphName := items[1]
	graph := findGraph(h.graphs, graphName)
	if graph == nil {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("not found")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	serviceName := items[2]
	service := findService(graph.services, serviceName)
	if service == nil || service.graph != nil {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("not found")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	query := req.URL.Query()
	countString := query.Get("count")
	if len(countString) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte("count not defined")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	count, err := strconv.Atoi(countString)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	if count <= 0 || count > 10000 {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte("count must be in [1,10000]")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	tasks := service.GetTasks(uint(count))
	data, err := json.Marshal(tasks)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(data); err != nil {
		logger.Printf("%s\n", err)
	}
}

func (h *HttpApi) submitResults(w http.ResponseWriter, req *http.Request, items []string) {
	graphName := items[1]
	graph := findGraph(h.graphs, graphName)
	if graph == nil {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("not found")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}
	serviceName := items[2]
	service := findService(graph.services, serviceName)
	if service == nil || service.graph != nil {
		w.WriteHeader(http.StatusNotFound)
		if _, err := w.Write([]byte("not found")); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}

	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		logger.Printf("%s\n", err)
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}

	var results []ServiceResult
	if err := json.Unmarshal(data, &results); err != nil {
		logger.Printf("%s\n", err)
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			logger.Printf("%s\n", err)
		}
		return
	}

	service.CommitTasks(results)

	w.WriteHeader(http.StatusOK)
	if _, err := w.Write([]byte("ok")); err != nil {
		logger.Printf("%s\n", err)
	}
}
