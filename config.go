package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

type Config struct {
	Defaults struct {
		Cache    map[string]string `json:"cache"`
		Storage  map[string]string `json:"storage"`
		Utilizer map[string]string `json:"utilizer"`
	} `json:"defaults"`
	Graphs []struct {
		Name    string            `json:"name"`
		Starts  []string          `json:"starts"`
		Cache   map[string]string `json:"cache"`
		Storage map[string]string `json:"storage"`
		Nodes   []struct {
			Name    string            `json:"name"`
			Timeout VertexTimeout     `json:"timeout"`
			Graph   string            `json:"graph"`
			Payload map[string]string `json:"payload"`
			Filter  map[string]string `json:"filter"`
			Links   []string          `json:"links"`
		} `json:"nodes"`
	} `json:"graphs"`
}

func (c *Config) parse(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(data, c); err != nil {
		return err
	}
	return nil
}

func (c *Config) getCache(values map[string]string) (VertexStorage, error) {
	cacheType := values["type"]
	if cacheType == "memory" {
		return NewMemoryVertexStorage(), nil
	}
	return nil, fmt.Errorf("cache %s not found", cacheType)
}

func (c *Config) getStorage(values map[string]string) (ServiceStorage, error) {
	storageType := values["type"]
	if storageType == "memory" {
		return NewMemoryServiceStorage(), nil
	}
	return nil, fmt.Errorf("storage %s not found", storageType)
}

func (c *Config) getUtilizer(values map[string]string) (Utilizer, error) {
	utilizerType := values["type"]
	if utilizerType == "stdout" {
		return NewUtilizeWriter(os.Stdout), nil
	}
	return nil, fmt.Errorf("utilizer %s not found", utilizerType)
}

func (c *Config) getPayload(values map[string]string) (PayloadMiddleware, error) {
	payloadType := values["type"]
	if payloadType == "lua" {
		name, ok := values["name"]
		if !ok {
			return nil, errors.New("function name is not defined")
		}
		filename, ok := values["filename"]
		if !ok {
			return nil, errors.New("filename is not defined")
		}
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			return nil, err
		}
		return NewLuaMiddleware(name, string(data))
	}
	return nil, fmt.Errorf("payload %s not found", payloadType)
}

func (c *Config) getFilter(values map[string]string) (FilterMiddleware, error) {
	filterType := values["type"]
	if filterType == "lua" {
		name, ok := values["name"]
		if !ok {
			return nil, errors.New("function name is not defined")
		}
		filename, ok := values["filename"]
		if !ok {
			return nil, errors.New("filename is not defined")
		}
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			return nil, err
		}
		return NewLuaMiddleware(name, string(data))
	}
	return nil, fmt.Errorf("filter %s not found", filterType)
}

func (c *Config) Build(dispatcherTimeout uint32, utilizerTimeout uint32) ([]Graph, error) {
	var cache VertexStorage
	var storage ServiceStorage
	var utilizer Utilizer
	var err error

	if c.Defaults.Cache != nil {
		cache, err = c.getCache(c.Defaults.Cache)
		if err != nil {
			return nil, err
		}
	}

	if c.Defaults.Storage != nil {
		storage, err = c.getStorage(c.Defaults.Storage)
		if err != nil {
			return nil, err
		}
	}

	if c.Defaults.Utilizer != nil {
		utilizer, err = c.getUtilizer(c.Defaults.Utilizer)
		if err != nil {
			return nil, err
		}
	}

	graphs := make([]Graph, 0)
	for _, graph := range c.Graphs {
		g := NewGraph(graph.Name, cache, storage)

		starts := make([]*VertexService, 0)
		nodes := make([]*VertexService, 0)
		services := make([]Service, 0)
		for _, node := range graph.Nodes {
			vertex := NewVertexService(node.Name, g, cache, node.Timeout)

			for _, name := range graph.Starts {
				if vertex.name == name {
					starts = append(g.starts, vertex)
				}
			}

			var payload PayloadMiddleware
			var filter FilterMiddleware

			if node.Payload != nil {
				payload, err = c.getPayload(node.Payload)
				if err != nil {
					logger.Fatalf("%s\n", err)
				}
			}

			if node.Filter != nil {
				filter, err = c.getFilter(node.Filter)
				if err != nil {
					logger.Fatalf("%s\n", err)
				}
			}

			service := NewService(vertex.name, vertex, storage, filter, payload)

			if len(node.Graph) > 0 {

				remoteGraph := findGraph(graphs, node.Graph)
				if remoteGraph == nil {
					return nil, fmt.Errorf("graph %s not defined", node.Graph)
				}
				vertex.SetTimeout(dispatcherTimeout, remoteGraph.GetMaxExecutionTime())
				service.graph = remoteGraph
			}

			services = append(services, *service)
			nodes = append(nodes, vertex)
		}

		g.starts = starts
		g.nodes = nodes
		g.services = services

		for _, node := range graph.Nodes {
			vertex := findVertex(g.nodes, node.Name)
			if vertex == nil {
				return nil, fmt.Errorf("vertex %s not found", node.Name)
			}
			for _, link := range node.Links {
				linkedVertex := findVertex(g.nodes, link)
				if linkedVertex == nil {
					return nil, fmt.Errorf("vertex %s not found", link)
				}
				linkedVertex.AddParent(vertex)
			}
		}

		if utilizer == nil {
			utilizer = NewUtilizeWriter(ioutil.Discard)
		}

		g.SetUtilizer(utilizer, utilizerTimeout)

		g.SetNewTimeouts()

		graphs = append(graphs, *g)
	}

	return graphs, nil

}
