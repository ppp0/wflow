package main

type Utilizer interface {
	Utilize(tasks []Task) (uint, error)
}
