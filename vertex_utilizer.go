package main

type VertexUtilizer struct {
	name    string
	timeout VertexTimeout

	graph *Graph

	new  *TaskContainerNew
	wait *TaskContainerWait
}

func NewVertexUtilizer(timeout VertexTimeout) *VertexUtilizer {
	v := new(VertexUtilizer)
	v.new = NewTaskContainerNew(timeout.New)
	v.wait = NewTaskContainerWait(timeout.Wait)
	v.timeout = timeout
	return v
}

func (v *VertexUtilizer) InsertTasks(tasks []VertexTask) {
	done := v.new.Insert(tasks)

	if len(done) > 0 {
		v.wait.Insert(done)
	}
}

func (v *VertexUtilizer) GetTasks(count uint) []VertexTask {
	return v.wait.Get(count)
}

func (v *VertexUtilizer) CommitTasks(tasks []VertexTask) {

}

func (v *VertexUtilizer) CollectGarbage() []VertexTask {
	var garbage []VertexTask
	v.new.CollectGarbage(&garbage)
	v.wait.CollectGarbage(&garbage)
	return garbage
}

func (v *VertexUtilizer) GraphName() string {
	return v.graph.name
}

func (v *VertexUtilizer) Name() string {
	return v.name
}

func (v *VertexUtilizer) GetMaxExecutionTime() uint32 {
	return v.new.ttl + v.wait.ttl
}

func (v *VertexUtilizer) SetNewTimeout(ttl uint32) uint32 {
	if ttl > v.new.ttl {
		v.new.ttl = ttl
	}
	return v.GetMaxExecutionTime()
}

func (v *VertexUtilizer) Childs() []Vertex {
	return nil
}
