package main

import (
	"github.com/rs/xid"
	"sync"
)

type MemoryVertexTask struct {
	task  VertexTask
	ttl   uint32
	state TaskState
}

type MemoryVertexStorage struct {
	items map[string]MemoryVertexTask
	sync.Mutex
}

func NewMemoryVertexStorage() *MemoryVertexStorage {
	m := new(MemoryVertexStorage)
	m.items = make(map[string]MemoryVertexTask)
	return m
}

func (s *MemoryVertexStorage) Put(graph string, vertex string, tasks []VertexTask, ttl uint32, state TaskState) {
	s.Lock()
	defer s.Unlock()

	for _, task := range tasks {
		key := graph + ":" + vertex + ":" + task.ID.String()
		s.items[key] = MemoryVertexTask{task, ttl, state}
	}
}

func (s *MemoryVertexStorage) Delete(graph string, vertex string, tasks []VertexTask) {
	s.Lock()
	defer s.Unlock()

	for _, task := range tasks {
		key := graph + ":" + vertex + ":" + task.ID.String()
		delete(s.items, key)
	}
}

type MemoryServiceStorage struct {
	items map[string]Task
	sync.Mutex
}

func NewMemoryServiceStorage() *MemoryServiceStorage {
	m := new(MemoryServiceStorage)
	m.items = make(map[string]Task)
	return m
}

func (s *MemoryServiceStorage) Create(graph string, requests []TaskRequest) ([]VertexTask, []TaskRequest) {
	s.Lock()
	defer s.Unlock()

	vertexTasks := make([]VertexTask, 0, len(requests))
	failed := make([]TaskRequest, 0, len(requests))

	for _, request := range requests {
		task := Task{}
		task.ID = xid.New()
		task.Payload = request.Payload
		task.Args = request.Args
		task.Services = make(map[string]string)
		task.ParentID = request.ParentID
		task.ParentVertex = request.ParentVertex
		task.ParentGraph = request.ParentGraph
		key := graph + ":" + task.ID.String()
		s.items[key] = task

		vertexTask := VertexTask{}
		vertexTask.ID = task.ID
		vertexTask.Priority = request.Args.Priority
		vertexTasks = append(vertexTasks, vertexTask)
	}

	return vertexTasks, failed
}

func (s *MemoryServiceStorage) Get(graph string, vertexTasks []VertexTask) ([]Task, []VertexTask) {
	s.Lock()
	defer s.Unlock()

	tasks := make([]Task, 0, len(vertexTasks))
	fails := make([]VertexTask, 0, len(vertexTasks))

	for _, vertexTask := range vertexTasks {
		key := graph + ":" + vertexTask.ID.String()
		task, ok := s.items[key]
		if !ok {
			fails = append(fails, vertexTask)
		} else {
			tasks = append(tasks, task)
		}
	}

	return tasks, fails
}

func (s *MemoryServiceStorage) Update(graph string, vertex string, results []ServiceResult) []VertexTask {
	s.Lock()
	defer s.Unlock()

	tasks := make([]VertexTask, 0, len(results))

	for _, result := range results {
		key := graph + ":" + result.ID
		item, ok := s.items[key]
		if ok {
			item.Services[vertex] = result.Result
			s.items[key] = item
		}

		var err error
		task := VertexTask{}
		task.ID, err = xid.FromString(result.ID)
		if err != nil {
			//
		} else {
			tasks = append(tasks, task)
		}
	}

	return tasks
}

func (s *MemoryServiceStorage) Extract(graph string, vertexTasks []VertexTask) []Task {
	s.Lock()
	defer s.Unlock()

	tasks := make([]Task, 0, len(vertexTasks))

	for _, vertexTask := range vertexTasks {
		key := graph + ":" + vertexTask.ID.String()
		task, ok := s.items[key]
		if ok {
			tasks = append(tasks, task)
			delete(s.items, key)
		}
	}

	return tasks
}
