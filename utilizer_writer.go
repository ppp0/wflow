package main

import (
	"encoding/json"
	"io"
)

type UtilizeWriter struct {
	writer io.Writer
}

func NewUtilizeWriter(writer io.Writer) *UtilizeWriter {
	u := UtilizeWriter{writer}
	return &u
}

func (u *UtilizeWriter) Utilize(tasks []Task) (uint, error) {
	enc := json.NewEncoder(u.writer)
	if err := enc.Encode(tasks); err != nil {
		return 0, err
	}
	return uint(len(tasks)), nil
}
