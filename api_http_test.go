package main

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func testHttpApiNewGraph(graphName string, vertexName string) Graph {
	cache := NewMemoryVertexStorage()
	storage := NewMemoryServiceStorage()

	graph := NewGraph(graphName, cache, storage)
	vertex := NewVertexService(vertexName, graph, cache, VertexTimeout{})
	service := NewService(vertexName, vertex, storage, nil, nil)
	graph.nodes = append(graph.nodes, vertex)
	graph.starts = append(graph.starts, vertex)
	graph.services = append(graph.services, *service)

	graph.SetUtilizer(NewUtilizeWriter(ioutil.Discard), 10)

	requests := make([]TaskRequest, 0)
	for i := 0; i < 100; i++ {
		request := TaskRequest{}
		request.Payload = "payload"
		requests = append(requests, request)
	}

	graph.CreateTasks(requests)

	return *graph
}

func testNewHttpApiCreateTasks(t *testing.T) {
	graphs := []Graph{testHttpApiNewGraph("graph1", "node1")}

	apiHandler := NewHttpApi(graphs)

	data := []byte(`[{"payload": "test payload"}]`)

	req, err := http.NewRequest("POST", "/graphs/graph1", bytes.NewReader(data))
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	rr := httptest.NewRecorder()

	apiHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("status (%d) != 200\n%s\n", rr.Code, rr.Body.String())
	}
}

func testNewHttpApiGetTasks(t *testing.T) {
	graphs := []Graph{testHttpApiNewGraph("graph1", "node1")}

	apiHandler := NewHttpApi(graphs)

	req, err := http.NewRequest("GET", "/graphs/graph1/services/node1?count=100", nil)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	rr := httptest.NewRecorder()

	apiHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("status (%d) != 200\n%s\n", rr.Code, rr.Body.String())
	}
}

func testNewHttpApiSubmitResults(t *testing.T) {
	graphs := []Graph{testHttpApiNewGraph("graph1", "node1")}

	apiHandler := NewHttpApi(graphs)

	data := []byte(`[{"id": "123", "result": "service result"}]`)

	req, err := http.NewRequest("POST", "/graphs/graph1/services/node1", bytes.NewReader(data))
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	rr := httptest.NewRecorder()

	apiHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("status (%d) != 200\n%s\n", rr.Code, rr.Body.String())
	}
}

func testNewHttpApiNotExistedEndpoint(t *testing.T) {
	graphs := []Graph{testHttpApiNewGraph("graph1", "node1")}

	apiHandler := NewHttpApi(graphs)

	req, err := http.NewRequest("GET", "/not_existed_endpoint/graph1", nil)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	rr := httptest.NewRecorder()

	apiHandler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("status (%d) != 404\n", rr.Code)
	}
}

func TestHttpApi_ServeHTTP(t *testing.T) {
	t.Run("not existed endpoint", testNewHttpApiNotExistedEndpoint)
	t.Run("create tasks", testNewHttpApiCreateTasks)
	t.Run("get tasks", testNewHttpApiGetTasks)
	t.Run("submit results", testNewHttpApiSubmitResults)
}
