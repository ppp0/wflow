package main

type VertexStorage interface {
	Put(graph string, vertex string, tasks []VertexTask, ttl uint32, state TaskState)
	Delete(graph string, vertex string, tasks []VertexTask)
}

type ServiceStorage interface {
	Create(graph string, tasks []TaskRequest) ([]VertexTask, []TaskRequest)
	Update(graph string, vertex string, results []ServiceResult) []VertexTask
	Get(graph string, tasks []VertexTask) ([]Task, []VertexTask)
	Extract(graph string, tasks []VertexTask) []Task
}
