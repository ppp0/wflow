package main

import (
	"github.com/rs/xid"
	"testing"
	"time"
)

func TestTaskContainerRun_Insert(t *testing.T) {
	container := NewTaskContainerRun(1)

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		tasks = append(tasks, task)
	}
	container.Insert(tasks)

	if len(container.items) != 100 {
		t.Errorf("count tasks (%d) != 100", len(container.items))
	}

}

func TestTaskContainerRun_Remove(t *testing.T) {
	container := NewTaskContainerRun(1)

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		task.Priority = 1
		tasks = append(tasks, task)
	}
	container.Insert(tasks)

	for _, task := range tasks {
		task.Priority = 2
	}

	container.Remove(tasks)

	for _, task := range tasks {
		if task.Priority != 1 {
			t.Errorf("priority (%d) != 1\n", task.Priority)
		}
	}

	if len(container.items) != 0 {
		t.Errorf("tasks in container (%d) != 0\n", len(container.items))
	}

}

func TestTaskContainerRun_CollectGarbage(t *testing.T) {
	container := NewTaskContainerRun(1)

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		tasks = append(tasks, task)
	}

	container.Insert(tasks)

	time.Sleep(2 * time.Second)

	garbage := make([]VertexTask, 0)
	container.CollectGarbage(&garbage)

	if len(garbage) != 100 {
		t.Errorf("garbage (%d) != 100", len(garbage))
	}

	if len(container.items) != 0 {
		t.Errorf("tasks in container (%d) != 0", len(garbage))
	}
}
