package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	logger *log.Logger
)

func garbageCollector(graphs []Graph, sleepTime time.Duration) {
	for {
		time.Sleep(sleepTime)
		for _, graph := range graphs {
			graph.CollectGarbage()
		}
	}
}

func graphDispatcher(graphs []Graph, sleepTime time.Duration, maxCount uint) {
	for {
		time.Sleep(sleepTime)

		for _, graph := range graphs {
			for _, service := range graph.services {
				if service.graph == nil {
					continue
				}

				tasks := service.GetTasks(maxCount)
				if len(tasks) == 0 {
					continue
				}

				var requests []TaskRequest
				var err error
				// var errorTasks []ErrorTask

				if service.payload == nil {
					requests = make([]TaskRequest, 0, len(tasks))

					for _, task := range tasks {
						request := TaskRequest{}
						request.ParentVertex = service.name
						request.ParentGraph = graph.name
						request.ParentID = task.ID
						request.Args = task.Args

						// process payload middleware
						request.Payload = task.Payload

						requests = append(requests, request)
					}
				} else {
					requests, _, err = service.payload.Payload(graph.name, service.name, tasks)
				}

				if err != nil {
					logger.Printf("%s\n", err)
				} else {
					service.graph.CreateTasks(requests)
				}
			}
		}
	}
}

func taskUtilizer(graphs []Graph, sleepTime time.Duration, maxCount uint) {
	for {
		time.Sleep(sleepTime)
		for _, graph := range graphs {
			for {
				vertexTasks := graph.end.GetTasks(maxCount)
				if len(vertexTasks) == 0 {
					break
				}

				tasks := graph.storage.Extract(graph.name, vertexTasks)

				returnedTasks := make(map[string]map[string][]ServiceResult)

				for _, task := range tasks {
					if len(task.ParentGraph) > 0 && len(task.ParentVertex) > 0 {
						data, err := json.Marshal(task)
						if err != nil {
							logger.Printf("%s\n", err)
						} else {
							graph, ok := returnedTasks[task.ParentGraph]
							if !ok {
								graph = make(map[string][]ServiceResult)
								returnedTasks[task.ParentGraph] = graph
							}
							service, ok := graph[task.ParentVertex]
							if !ok {
								service = make([]ServiceResult, 0)
								graph[task.ParentVertex] = service
							}
							service = append(service, ServiceResult{ID: task.ParentID.String(), Result: string(data)})
							graph[task.ParentVertex] = service
						}
					}
				}

				for graphName, graph := range returnedTasks {
					remoteGraph := findGraph(graphs, graphName)
					if remoteGraph == nil {
						logger.Printf("remote graph %s not found\n", graphName)
						continue
					}
					for serviceName, results := range graph {
						remoteService := findService(remoteGraph.services, serviceName)
						if remoteService == nil {
							logger.Printf("remote service %s not found\n", serviceName)
							continue
						}
						remoteService.CommitTasks(results)
					}
				}

				if len(tasks) > 0 {
					var err error
					_, err = graph.utilizer.Utilize(tasks)
					if err != nil {
						logger.Printf("%s\n", err)
					}
				}
			}
		}
	}
}

func run(graphs []Graph, httpAddr string) {
	go garbageCollector(graphs, 1000*time.Millisecond)
	go taskUtilizer(graphs, 1000*time.Millisecond, 10000)
	go graphDispatcher(graphs, 3000*time.Millisecond, 10000)

	httpApi := NewHttpApi(graphs)

	if err := http.ListenAndServe(httpAddr, httpApi); err != nil {
		panic(err)
	}
}

func init() {
	logger = log.New(os.Stderr, "", log.Ldate|log.Lshortfile)
}

func main() {
	filename := flag.String("config", "config.json", "config file name")
	httpAddr := flag.String("http-addr", "127.0.0.1:5555", "http api bind addr")
	flag.Parse()

	cfg := new(Config)
	if err := cfg.parse(*filename); err != nil {
		logger.Fatalf("%s\n", err)
	}

	// FIXME: fixed time
	graphs, err := cfg.Build(10, 10)
	if err != nil {
		logger.Fatalf("%s\n", err)
	}

	run(graphs, *httpAddr)
}
