package main

type Graph struct {
	name     string
	services []Service
	nodes    []*VertexService
	starts   []*VertexService
	end      *VertexUtilizer

	cache    VertexStorage
	storage  ServiceStorage
	utilizer Utilizer
}

func NewGraph(name string, cache VertexStorage, storage ServiceStorage) *Graph {
	g := new(Graph)
	g.name = name
	g.cache = cache
	g.storage = storage
	return g
}

func (g *Graph) Name() string {
	return g.name
}

func (g *Graph) CreateTasks(requests []TaskRequest) []VertexTask {

	vertexTasks, _ := g.storage.Create(g.name, requests)

	for _, start := range g.starts {
		start.InsertTasks(vertexTasks)
	}

	return vertexTasks
}

func (g *Graph) SetUtilizer(utilizer Utilizer, waitTimeout uint32) {
	timeout := VertexTimeout{New: 1, Wait: waitTimeout}
	g.end = NewVertexUtilizer(timeout)

	for _, node := range g.nodes {
		if len(node.childs) == 0 {
			node.childs = append(node.childs, g.end)
		}
	}

	g.utilizer = utilizer

}

func (g *Graph) CollectGarbage() uint {
	count := uint(0)
	for _, node := range g.nodes {
		garbage := node.CollectGarbage()
		if len(garbage) > 0 {
			count += uint(len(garbage))
			logger.Printf("removed %d\n", len(garbage))
		}
	}
	return count
}

func (g *Graph) traverse(prevTime uint32, vertex Vertex) {
	newTime := vertex.SetNewTimeout(prevTime)

	for _, child := range vertex.Childs() {
		g.traverse(newTime, child)
	}
}

func (g *Graph) SetNewTimeouts() {
	for _, start := range g.starts {
		g.traverse(1, start)
	}
}

func (g *Graph) GetMaxExecutionTime() uint32 {
	return g.end.GetMaxExecutionTime()
}
