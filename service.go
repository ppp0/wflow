package main

type Service struct {
	name string

	vertex Vertex
	graph  *Graph

	storage ServiceStorage
	filter  FilterMiddleware
	payload PayloadMiddleware
}

func NewService(name string, vertex Vertex, storage ServiceStorage, filter FilterMiddleware, payload PayloadMiddleware) *Service {
	s := new(Service)
	s.name = name
	s.vertex = vertex
	s.storage = storage
	s.filter = filter
	s.payload = payload
	return s
}

func (s *Service) GetTasks(count uint) []Task {

	if count == 0 {
		return make([]Task, 0)
	}

	vertexTasks := s.vertex.GetTasks(count)

	if len(vertexTasks) == 0 {
		return make([]Task, 0)
	}

	tasks, _ := s.storage.Get(s.vertex.GraphName(), vertexTasks)

	if s.filter != nil {
		_, err := s.filter.Filter(&vertexTasks, &tasks)
		if err != nil {
			logger.Printf("%s\n", err)
			return make([]Task, 0)
		}
		if len(vertexTasks) > 0 {
			s.vertex.CommitTasks(vertexTasks)
		}
	}

	return tasks
}

func (s *Service) CommitTasks(results []ServiceResult) {

	tasks := s.storage.Update(s.vertex.GraphName(), s.vertex.Name(), results)

	if len(tasks) > 0 {
		s.vertex.CommitTasks(tasks)
	}

}
