package main

import (
	"sync"
	"time"
)

const (
	TaskMaxPriority = 16
)

type TaskContainerWait struct {
	items [TaskMaxPriority][]VertexTask
	ttl   uint32
	sync.Mutex
}

func NewTaskContainerWait(ttl uint32) *TaskContainerWait {
	c := new(TaskContainerWait)
	for i := 0; i < TaskMaxPriority; i++ {
		c.items[i] = make([]VertexTask, 0)
	}
	c.ttl = ttl
	return c
}

func (c *TaskContainerWait) Insert(tasks []VertexTask) {

	now := uint32(time.Now().Unix())

	c.Lock()
	defer c.Unlock()

	for i, task := range tasks {
		if task.Priority >= TaskMaxPriority {
			task.Priority = 15
		}
		task.TTL = now + c.ttl
		c.items[task.Priority] = append(c.items[task.Priority], task)
		tasks[i] = task
	}
}

func (c *TaskContainerWait) Get(count uint) []VertexTask {
	c.Lock()
	defer c.Unlock()

	tasks := make([]VertexTask, 0, count)

	for i := TaskMaxPriority - 1; i >= 0; i-- {
		copyCount := cap(tasks) - len(tasks)
		if copyCount == 0 {
			break
		}
		if copyCount > len(c.items[i]) {
			copyCount = len(c.items[i])
		}
		if copyCount == 0 {
			continue
		}
		tasks = append(tasks, c.items[i][:copyCount]...)
		c.items[i] = c.items[i][copyCount:]
	}

	return tasks
}

func (c *TaskContainerWait) CollectGarbage(tasks *[]VertexTask) {

	now := uint32(time.Now().Unix())

	c.Lock()
	defer c.Unlock()

	for i := 0; i < TaskMaxPriority; i++ {
		k := 0
		for _, task := range c.items[i] {
			if task.TTL <= now {
				*tasks = append(*tasks, task)
			} else {
				c.items[i][k] = task
				k++
			}
		}
		c.items[i] = c.items[i][:k]
	}
}
