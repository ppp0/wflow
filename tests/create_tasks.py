import requests

URL = "http://127.0.0.1:5555"

def create(count):
    items = []
    for i in range(count):
        items.append({"payload": "my_payload " + str(i)})
    r = requests.post(URL + "/graphs/graph1", json=items)
    assert r.status_code == 200, "code: %d" % r.status_code
    return r.json()

def main():
    return create(10)

if __name__ == "__main__":
    print(main())
