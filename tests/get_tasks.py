import requests

URL = "http://127.0.0.1:5555"

def get(count):
    r = requests.get(URL + "/graphs/graph1/services/node1?count=" + str(count))
    assert r.status_code == 200, "code: %d" % r.status_code
    return r.json()

def main():
    return get(100)

if __name__ == "__main__":
    print(main())
