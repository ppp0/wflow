import sys
import time
from random import randint
from typing import List
import threading
import json
import urllib.request


class WFlow(object):
    def __init__(self, addr):
        self.addr = addr

    def create_tasks(self, graph: str, tasks: List[str]):
        items = []
        for task in tasks:
            items.append({"payload": task})
        params = json.dumps(items).encode('utf8')
        req = urllib.request.Request(self.addr + "/graphs/" + graph, data=params,
                                     headers={'content-type': 'application/json'})
        response = urllib.request.urlopen(req)
        data = response.read().decode('utf8')
        assert response.status == 200, "Code: %d\nError: %s" % (response.status, data)
        return json.loads(data)

    def get_tasks(self, graph: str, service_name: str, count: int):
        req = urllib.request.Request(self.addr + "/graphs/" + graph + "/services/" + service_name + "?count="+str(count),
                                     headers={'content-type': 'application/json'})
        response = urllib.request.urlopen(req)
        data = response.read().decode('utf8')
        assert response.status == 200, "Code: %d\nError: %s" % (response.status, data)
        return json.loads(data)

    def submit_results(self, graph: str, service_name: str, results):
        params = json.dumps(results).encode('utf8')
        req = urllib.request.Request(self.addr + "/graphs/" + graph + "/services/" + service_name, data=params,
                                     headers={'content-type': 'application/json'})
        response = urllib.request.urlopen(req)
        data = response.read().decode('utf8')
        assert response.status == 200, "Code: %d\nError: %s" % (response.status, data)
        return


def service(url: str, graph: str, name: str):
    client = WFlow(url)
    print("run %s" % name)
    while True:
        try:
            tasks = client.get_tasks(graph, name, 100)
            print("get %d tasks from %s" % (len(tasks), name))
        except Exception as e:
            print(graph, name, str(e))
            time.sleep(1)
            continue
        time.sleep(1)
        if len(tasks) > 0:
            results = []
            for task in tasks:
                results.append({'id': task['id'], 'result': 'result service'})
            try:
                client.submit_results(graph, name, results)
                print("submit to %s %d tasks" % (name, len(results)))
            except Exception as e:
                print(graph, name, str(e))
        time.sleep(1)


def run_graph(url: str, name: str, services: List[str]):
    for service_name in services:
        thread_service = threading.Thread(target=service, args=(url, name, service_name))
        thread_service.start()


def build(filename, url):
    with open(filename) as f:
        config = json.loads(f.read())
    graphs = []
    for graph in config["graphs"]:
        services = []
        for node in graph["nodes"]:
            if "graph" not in node:
                services.append(node["name"])
        run_graph(url, graph["name"], services)
        graphs.append(graph["name"])
    return graphs


def run(filename, url):
    graphs = build(filename, url)
    client = WFlow(url)
    while True:
        try:
            for graph in graphs:
                tasks = []
                for i in range(randint(1, 10)):
                    tasks.append("payload %d" % i)
                tasks = client.create_tasks(graph, tasks)
                print("created", len(tasks))
            time.sleep(10)
        except:
            break


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("usage: %s config_file_path api_url" % sys.argv[0])
    else:
        run(sys.argv[1], sys.argv[2])
