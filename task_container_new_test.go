package main

import (
	"github.com/rs/xid"
	"testing"
	"time"
)

func TestTaskContainerNew_Insert(t *testing.T) {
	container := NewTaskContainerNew(1)
	container.parents = 2

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		tasks = append(tasks, task)
	}
	done := container.Insert(tasks)

	if len(done) != 0 {
		t.Errorf("done tasks (%d) != 0", len(done))
	}

	for _, task := range tasks {
		if task.Parents != 1 {
			t.Errorf("task parents (%d) != 1", task.Parents)
		}
	}

	done = container.Insert(tasks)

	if len(done) != 100 {
		t.Errorf("done tasks (%d) != 100", len(done))
	}

	container = NewTaskContainerNew(1)
	container.parents = 1

	tasks = make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		tasks = append(tasks, task)
	}

	done = container.Insert(tasks)

	if len(done) != 100 {
		t.Errorf("done tasks (%d) != 100", len(done))
	}

}

func TestTaskContainerNew_CollectGarbage(t *testing.T) {
	container := NewTaskContainerNew(1)
	container.parents = 3

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		task.Parents = 3
		tasks = append(tasks, task)
	}

	container.Insert(tasks)

	container.Insert(tasks)

	time.Sleep(2 * time.Second)

	garbage := make([]VertexTask, 0)
	container.CollectGarbage(&garbage)

	if len(garbage) != 100 {
		t.Errorf("garbage (%d) != 100", len(garbage))
	}

	if len(container.items) != 0 {
		t.Errorf("tasks in container (%d) != 0", len(garbage))
	}
}
