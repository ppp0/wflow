package main

import "github.com/rs/xid"

type TaskState uint8

const (
	TaskNew TaskState = iota + 1
	TaskWait
	TaskRun
)

type TaskArgs struct {
	Priority uint8 `json:"priority"`
}

type VertexTaskContext struct {
	TTL      uint32 `json:"ttl"`
	Parents  uint8  `json:"parents"`
	Priority uint8  `json:"priority"`
}

type VertexTask struct {
	ID xid.ID `json:"id"`
	VertexTaskContext
}

type Task struct {
	ID       xid.ID            `json:"id"`
	Args     TaskArgs          `json:"args"`
	Payload  string            `json:"payload"`
	Services map[string]string `json:"services"`

	ParentID     xid.ID `json:"parent_id"`
	ParentVertex string `json:"parent_vertex"`
	ParentGraph  string `json:"parent_graph"`
}

type ServiceResult struct {
	ID     string `json:"id"`
	Result string `json:"result"`
}

type TaskRequest struct {
	Payload string   `json:"payload"`
	Args    TaskArgs `json:"args"`

	ParentID     xid.ID `json:"parent_id"`
	ParentVertex string `json:"parent_vertex"`
	ParentGraph  string `json:"parent_graph"`
}

type ErrorTask struct {
	Task
	Error string
}
