package main

import (
	"encoding/json"
	"testing"
)

func testConfigNoRemoteGraph(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "memory"
    },
    "storage": {
      "type": "memory"
    },
    "utilizer": {
      "type": "stdout"
    }
  },
  "graphs": [
    {
      "name": "graph3",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 5
          },
          "links": ["node2"]
        },
        {
          "name": "node2",
          "graph": "not_existed_graph",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err == nil {
		t.Fatal("err == nil\n")
	}

	if graphs != nil {
		t.Errorf("err == nil\n")
	}

	if err.Error() != "graph not_existed_graph not defined" {
		t.Errorf("err is incorrect (%s)\n", err)
	}

}

func testConfigNoUtilizer(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "memory"
    },
    "storage": {
      "type": "memory"
    }
  },
  "graphs": [
    {
      "name": "graph2",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 5
          },
          "links": ["node2"]
        },
        {
          "name": "node2",
          "graph": "graph2",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err != nil {
		t.Fatalf("err != nil, %s\n", err)
	}

	for _, graph := range graphs {
		if graph.utilizer == nil {
			t.Errorf("utilizer == nil\n")
		}
	}
}

func testConfigNoLinkedVertex(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "memory"
    },
    "storage": {
      "type": "memory"
    },
    "utilizer": {
      "type": "stdout"
    }
  },
  "graphs": [
    {
      "name": "graph2",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 5
          },
          "links": ["not_existed_node"]
        },
        {
          "name": "node2",
          "graph": "graph2",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err == nil {
		t.Fatal("err == nil\n")
	}

	if graphs != nil {
		t.Errorf("graphs != nil\n")
	}

	if err.Error() != "vertex not_existed_node not found" {
		t.Errorf("err is incorrect (%s)\n", err)
	}
}

func testConfigNoExistedUtilizer(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "memory"
    },
    "storage": {
      "type": "memory"
    },
    "utilizer": {
      "type": "not_existed_utilizer"
    }
  },
  "graphs": [
    {
      "name": "graph2",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 5
          },
          "links": ["node2"]
        },
        {
          "name": "node2",
          "graph": "graph2",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err == nil {
		t.Fatal("err == nil\n")
	}

	if graphs != nil {
		t.Errorf("graphs != nil\n")
	}

	if err.Error() != "utilizer not_existed_utilizer not found" {
		t.Errorf("err is incorrect (%s)\n", err)
	}
}

func testConfigNoExistedCache(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "not_existed_cache"
    },
    "storage": {
      "type": "memory"
    },
    "utilizer": {
      "type": "stdout"
    }
  },
  "graphs": [
    {
      "name": "graph2",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 5
          },
          "links": ["node2"]
        },
        {
          "name": "node2",
          "graph": "graph2",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err == nil {
		t.Fatal("err == nil\n")
	}

	if graphs != nil {
		t.Errorf("graphs != nil\n")
	}

	if err.Error() != "cache not_existed_cache not found" {
		t.Errorf("err is incorrect (%s)\n", err)
	}
}

func testConfigNoExistedStorage(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "memory"
    },
    "storage": {
      "type": "not_existed_storage"
    },
    "utilizer": {
      "type": "stdout"
    }
  },
  "graphs": [
    {
      "name": "graph2",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 5
          },
          "links": ["node2"]
        },
        {
          "name": "node2",
          "graph": "graph2",
          "timeout": {
            "new": 1,
            "wait": 10,
            "run": 10
          }
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err == nil {
		t.Fatal("err == nil\n")
	}

	if graphs != nil {
		t.Errorf("graphs != nil\n")
	}

	if err.Error() != "storage not_existed_storage not found" {
		t.Errorf("err is incorrect (%s)\n", err)
	}
}

func testConfigBuild(t *testing.T) {
	data := `
{
  "defaults": {
    "cache": {
      "type": "memory"
    },
    "storage": {
      "type": "memory"
    },
    "utilizer": {
      "type": "stdout"
    }
  },
  "graphs": [
    {
      "name": "graph2",
      "starts": ["node11"],
      "nodes": [
        {
          "name": "node11",
          "timeout": {
            "wait": 10,
            "run": 10
          }
        }
      ]
    },
    {
      "name": "graph1",
      "starts": ["node1"],
      "nodes": [
        {
          "name": "node1",
          "timeout": {
            "wait": 10,
            "run": 5
          },
          "links": ["node2"]
        },
        {
          "name": "node2",
          "graph": "graph2"
        }
      ]
    }
  ]
}
`
	cfg := new(Config)
	if err := json.Unmarshal([]byte(data), &cfg); err != nil {
		t.Fatalf("%s\n", err)
	}

	graphs, err := cfg.Build(10, 10)
	if err != nil {
		t.Fatalf("err != nil, %s\n", err)
	}

	if len(graphs) != 2 {
		t.Errorf("count graphs (%d) != 2\n", len(graphs))
	}

	if graphs[0].GetMaxExecutionTime() != 31 {
		t.Error("max execution time graph[0] != 31\n")
	}

	if graphs[1].GetMaxExecutionTime() != 67 {
		t.Error("max execution time graph[0] != 67\n")
	}
}

func TestConfig_Build(t *testing.T) {
	t.Run("build", testConfigBuild)
	t.Run("no utilizer", testConfigNoUtilizer)
	t.Run("no remote graph", testConfigNoRemoteGraph)
	t.Run("no linked vertex", testConfigNoLinkedVertex)
	t.Run("not existed utilizer", testConfigNoExistedUtilizer)
	t.Run("not existed cache", testConfigNoExistedCache)
	t.Run("not existed storage", testConfigNoExistedStorage)
}
