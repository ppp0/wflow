package main

import (
	"fmt"
	lua "github.com/yuin/gopher-lua"
)

type LuaMiddleware struct {
	code string
	name string
}

func NewLuaMiddleware(name string, code string) (*LuaMiddleware, error) {
	L := lua.NewState()
	defer L.Close()

	L.OpenLibs()

	if err := L.DoString(code); err != nil {
		return nil, err
	}

	function := L.GetGlobal(name)
	if function.Type() != lua.LTFunction {
		return nil, fmt.Errorf("%s is not lua function", name)
	}

	m := new(LuaMiddleware)
	m.code = code
	m.name = name
	return m, nil
}

func (l *LuaMiddleware) Filter(vertexTasks *[]VertexTask, tasks *[]Task) ([]ErrorTask, error) {
	L := lua.NewState()
	defer L.Close()

	L.OpenLibs()

	if err := L.DoString(l.code); err != nil {
		return nil, err
	}

	errorTasks := make([]ErrorTask, 0, len(*tasks))

	for i, j := 0, 0; i < len(*tasks); i, j = i+1, j+1 {
		task := (*tasks)[i]

		L.Push(L.GetGlobal(l.name))
		L.Push(lua.LString(task.Payload))

		table := L.NewTable()
		for name, value := range task.Services {
			L.SetField(table, name, lua.LString(value))
		}
		L.Push(table)

		if err := L.PCall(2, 1, nil); err != nil {
			*tasks = append((*tasks)[:i], (*tasks)[i+1:]...)
			i--
			*vertexTasks = append((*vertexTasks)[:j], (*vertexTasks)[j+1:]...)
			j--
			errorTasks = append(errorTasks, ErrorTask{task, err.Error()})
			task = Task{}
		} else {
			value := L.Get(-1)
			if value.Type() != lua.LTBool {
				*tasks = append((*tasks)[:i], (*tasks)[i+1:]...)
				i--
				*vertexTasks = append((*vertexTasks)[:j], (*vertexTasks)[j+1:]...)
				j--
				errorTasks = append(errorTasks, ErrorTask{task, "return value is not bool"})
				task = Task{}
			} else {
				if value == lua.LTrue {
					*tasks = append((*tasks)[:i], (*tasks)[i+1:]...)
					task = Task{}
					i--
				} else {
					*vertexTasks = append((*vertexTasks)[:j], (*vertexTasks)[j+1:]...)
					j--
				}
			}
		}
	}

	return errorTasks, nil
}

func (l *LuaMiddleware) Payload(graph string, vertex string, tasks []Task) ([]TaskRequest, []ErrorTask, error) {
	L := lua.NewState()
	defer L.Close()

	L.OpenLibs()

	if err := L.DoString(l.code); err != nil {
		return nil, nil, err
	}

	errorTasks := make([]ErrorTask, 0, len(tasks))
	requests := make([]TaskRequest, 0, len(tasks))

	for _, task := range tasks {
		L.Push(L.GetGlobal(l.name))
		L.Push(lua.LString(task.Payload))

		table := L.NewTable()
		for name, value := range task.Services {
			L.SetField(table, name, lua.LString(value))
		}
		L.Push(table)

		if err := L.PCall(2, 1, nil); err != nil {
			errorTasks = append(errorTasks, ErrorTask{task, err.Error()})
		} else {
			value := L.Get(-1)
			if value.Type() != lua.LTString {
				errorTasks = append(errorTasks, ErrorTask{task, "return value is not string"})
			} else {
				request := TaskRequest{}
				request.Payload = value.String()
				request.ParentVertex = vertex
				request.ParentGraph = graph
				request.ParentID = task.ID
				request.Args = task.Args

				requests = append(requests, request)
			}
		}
	}

	return requests, errorTasks, nil

}
