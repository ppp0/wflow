package main

import (
	"github.com/rs/xid"
	"io/ioutil"
	"testing"
)

func TestUtilizeWriter_Utilize(t *testing.T) {
	utilizer := NewUtilizeWriter(ioutil.Discard)

	tasks := make([]Task, 0)
	for i := 0; i < 100; i++ {
		task := Task{}
		task.ID = xid.New()
		task.Payload = "payload"
		tasks = append(tasks, task)
	}

	count, err := utilizer.Utilize(tasks)

	if err != nil {
		t.Errorf("%s\n", err)
	}

	if count != 100 {
		t.Errorf("utilized count (%d) != 100\n", count)
	}
}
