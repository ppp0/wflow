package main

import (
	"io/ioutil"
	"testing"
	"time"
)

func TestGraph_CreateTasks(t *testing.T) {
	cache := NewMemoryVertexStorage()
	storage := NewMemoryServiceStorage()

	graph := NewGraph("graph", cache, storage)
	vertex := NewVertexService("vertex", graph, cache, VertexTimeout{})
	graph.nodes = append(graph.nodes, vertex)
	graph.starts = append(graph.starts, vertex)

	graph.SetUtilizer(NewUtilizeWriter(ioutil.Discard), 10)

	requests := make([]TaskRequest, 0)
	for i := 0; i < 100; i++ {
		request := TaskRequest{}
		request.Payload = "payload"
		requests = append(requests, request)
	}

	tasks := graph.CreateTasks(requests)

	if len(tasks) != 100 {
		t.Errorf("count tasks (%d) != 100\n", len(tasks))
	}

}

func TestGraph_CollectGarbage(t *testing.T) {
	cache := NewMemoryVertexStorage()
	storage := NewMemoryServiceStorage()

	graph := NewGraph("graph", cache, storage)
	vertex := NewVertexService("vertex", graph, cache, VertexTimeout{1, 1, 1})
	graph.nodes = append(graph.nodes, vertex)
	graph.starts = append(graph.starts, vertex)

	graph.SetUtilizer(NewUtilizeWriter(ioutil.Discard), 10)

	requests := make([]TaskRequest, 0)
	for i := 0; i < 100; i++ {
		request := TaskRequest{}
		request.Payload = "payload"
		requests = append(requests, request)
	}

	graph.CreateTasks(requests)

	time.Sleep(2 * time.Second)

	if count := graph.CollectGarbage(); count != 100 {
		t.Errorf("count garbage (%d) != 100\n", count)
	}
}
