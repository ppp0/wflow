package main

import (
	"github.com/rs/xid"
	"testing"
	"time"
)

func TestTaskContainerWait_Insert(t *testing.T) {
	container := NewTaskContainerWait(1)

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		task.Priority = uint8(i)
		tasks = append(tasks, task)
	}
	container.Insert(tasks)

	count := 0
	for i := 0; i < TaskMaxPriority; i++ {
		count += len(container.items[i])
	}

	if count != 100 {
		t.Errorf("count (%d) != 100", count)
	}
}

func TestTaskContainerWait_Get(t *testing.T) {
	container := NewTaskContainerWait(1)

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		task.Priority = uint8(i)
		tasks = append(tasks, task)
	}

	tasks2 := container.Get(10)

	if len(tasks2) != 0 {
		t.Errorf("count tasks (%d) != 0", len(tasks2))
	}

	container.Insert(tasks)

	tasks = container.Get(84)

	if len(tasks) != 84 {
		t.Errorf("count tasks (%d) != 84", len(tasks))
	}

	count := 0
	for i := 0; i < TaskMaxPriority; i++ {
		count += len(container.items[i])
	}

	if count != 16 {
		t.Errorf("count (%d) != 16", len(tasks))
	}

	tasks = container.Get(16)

	for i := 0; i < TaskMaxPriority; i++ {
		if tasks[i].Priority != uint8(TaskMaxPriority-(i+1)) {
			t.Errorf("priority order error, %d != %d\n", tasks[i].Priority, uint8(TaskMaxPriority-(i+1)))
		}
	}

}

func TestTaskContainerWait_CollectGarbage(t *testing.T) {
	container := NewTaskContainerWait(2)

	tasks := make([]VertexTask, 0)
	for i := 0; i < 100; i++ {
		task := VertexTask{ID: xid.New()}
		task.Priority = uint8(i)
		tasks = append(tasks, task)
	}

	container.Insert(tasks)

	time.Sleep(1 * time.Second)
	garbage := make([]VertexTask, 0)
	container.CollectGarbage(&garbage)

	if len(garbage) != 0 {
		t.Errorf("garbage (%d) != 0", len(garbage))
	}

	time.Sleep(2 * time.Second)

	garbage = make([]VertexTask, 0)
	container.CollectGarbage(&garbage)

	if len(garbage) != 100 {
		t.Errorf("garbage (%d) != 100", len(garbage))
	}

	count := 0
	for i := 0; i < TaskMaxPriority; i++ {
		count += len(container.items[i])
	}

	if count != 0 {
		t.Errorf("tasks in container (%d) != 0", count)
	}
}
