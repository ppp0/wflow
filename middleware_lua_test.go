package main

import (
	"github.com/rs/xid"
	"strconv"
	"testing"
)

func testLuaMiddlewareFilterAll(t *testing.T) {
	code := `
function filter_task(payload, services)
return true
end
`

	middleware, err := NewLuaMiddleware("filter_task", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)
	vertexTasks := make([]VertexTask, 0)

	for i := 0; i < 100; i++ {
		vertexTask := VertexTask{}
		vertexTask.ID = xid.New()

		task := Task{}
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
		vertexTasks = append(vertexTasks, vertexTask)
	}
	errorTasks, err := middleware.Filter(&vertexTasks, &tasks)

	if err != nil {
		t.Fatalf("%s\n", err)
	}

	if len(errorTasks) != 0 {
		t.Errorf("errorTasks (%d) != 0", len(errorTasks))
	}

	if len(vertexTasks) != 100 {
		t.Errorf("vertexTasks (%d) != 100", len(vertexTasks))
	}

	if len(tasks) != 0 {
		t.Errorf("tasks (%d) != 0", len(tasks))
	}
}

func testLuaMiddlewareFilterNone(t *testing.T) {
	code := `
function filter_task(payload, services)
return false
end
`

	middleware, err := NewLuaMiddleware("filter_task", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)
	vertexTasks := make([]VertexTask, 0)

	for i := 0; i < 100; i++ {
		vertexTask := VertexTask{}
		vertexTask.ID = xid.New()

		task := Task{}
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
		vertexTasks = append(vertexTasks, vertexTask)
	}
	errorTasks, err := middleware.Filter(&vertexTasks, &tasks)

	if err != nil {
		t.Fatalf("%s\n", err)
	}

	if len(errorTasks) != 0 {
		t.Errorf("errorTasks (%d) != 0", len(errorTasks))
	}

	if len(vertexTasks) != 0 {
		t.Errorf("vertexTasks (%d) != 0", len(vertexTasks))
	}

	if len(tasks) != 100 {
		t.Errorf("tasks (%d) != 100", len(tasks))
	}
}

func testLuaMiddlewareFilterLuaError(t *testing.T) {
	code := `
function filter_task(payload, services)
error("test error")
return true
end
`

	middleware, err := NewLuaMiddleware("filter_task", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)
	vertexTasks := make([]VertexTask, 0)

	for i := 0; i < 100; i++ {
		vertexTask := VertexTask{}
		vertexTask.ID = xid.New()

		task := Task{}
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
		vertexTasks = append(vertexTasks, vertexTask)
	}
	errorTasks, err := middleware.Filter(&vertexTasks, &tasks)

	if err != nil {
		t.Fatalf("error (%s) not nil\n", err)
	}

	if len(errorTasks) != 100 {
		t.Errorf("errorTasks (%d) != 100\n", len(errorTasks))
	}

	if len(vertexTasks) != 0 {
		t.Errorf("vertexTasks (%d) != 0\n", len(vertexTasks))
	}

	if len(tasks) != 0 {
		t.Errorf("tasks (%d) != 0\n", len(tasks))
	}
}

func testLuaMiddlewareFilterInvalidReturnType(t *testing.T) {
	code := `
function filter_task(payload, services)
return "string"
end
`

	middleware, err := NewLuaMiddleware("filter_task", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)
	vertexTasks := make([]VertexTask, 0)

	for i := 0; i < 100; i++ {
		vertexTask := VertexTask{}
		vertexTask.ID = xid.New()

		task := Task{}
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
		vertexTasks = append(vertexTasks, vertexTask)
	}
	errorTasks, err := middleware.Filter(&vertexTasks, &tasks)

	if err != nil {
		t.Fatalf("error (%s) not nil\n", err)
	}

	if len(errorTasks) != 100 {
		t.Errorf("errorTasks (%d) != 100\n", len(errorTasks))
	}

	for _, errorTask := range errorTasks {
		if errorTask.Error != "return value is not bool" {
			t.Errorf("error (%s) != 100\n", "return value is not bool")
		}
	}

	if len(vertexTasks) != 0 {
		t.Errorf("vertexTasks (%d) != 0\n", len(vertexTasks))
	}

	if len(tasks) != 0 {
		t.Errorf("tasks (%d) != 0\n", len(tasks))
	}
}

func testLuaMiddlewareFuntionNotExist(t *testing.T) {
	code := `
function function_name(payload, services)
return true
end
`
	_, err := NewLuaMiddleware("function_name_not_exist", code)
	if err == nil {
		t.Errorf("function_name_not_exist exist in code")
	}
}

func testLuaMiddlewareSyntaxError(t *testing.T) {
	code := `
1_function function_name(payload, services)
return true
end
`
	_, err := NewLuaMiddleware("function_name", code)
	if err == nil {
		t.Errorf("error == nil")
	}

}

func testLuaMiddlewarePayloadLuaError(t *testing.T) {
	code := `
function make_payload(payload, services)
error("test error")
return true
end
`

	middleware, err := NewLuaMiddleware("make_payload", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)

	for i := 0; i < 100; i++ {
		task := Task{}
		task.ID = xid.New()
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
	}
	requests, errorTasks, err := middleware.Payload("graph", "vertex", tasks)

	if err != nil {
		t.Fatalf("error (%s) not nil\n", err)
	}

	if len(errorTasks) != 100 {
		t.Errorf("errorTasks (%d) != 100\n", len(errorTasks))
	}

	if len(requests) != 0 {
		t.Errorf("requests (%d) != 0\n", len(requests))
	}
}

func testLuaMiddlewarePayloadInvalidReturnType(t *testing.T) {
	code := `
function make_payload(payload, services)
return true
end
`

	middleware, err := NewLuaMiddleware("make_payload", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)

	for i := 0; i < 100; i++ {
		task := Task{}
		task.ID = xid.New()
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
	}
	requests, errorTasks, err := middleware.Payload("graph", "vertex", tasks)

	if err != nil {
		t.Fatalf("error (%s) not nil\n", err)
	}

	if len(errorTasks) != 100 {
		t.Errorf("errorTasks (%d) != 100\n", len(errorTasks))
	}

	for _, errorTask := range errorTasks {
		if errorTask.Error != "return value is not string" {
			t.Errorf("error (%s) != 100\n", "return value is not bool")
		}
	}

	if len(requests) != 0 {
		t.Errorf("vertexTasks (%d) != 0\n", len(requests))
	}
}

func testLuaMiddlewarePayload(t *testing.T) {
	code := `
function make_payload(payload, services)
return "new payload"
end
`

	middleware, err := NewLuaMiddleware("make_payload", code)
	if err != nil {
		t.Fatalf("%s\n", err)
	}

	tasks := make([]Task, 0)

	for i := 0; i < 100; i++ {
		task := Task{}
		task.ID = xid.New()
		task.Payload = "payload " + strconv.Itoa(i)
		task.Services = map[string]string{"service1": "1"}

		tasks = append(tasks, task)
	}
	requests, errorTasks, err := middleware.Payload("graph", "vertex", tasks)

	if err != nil {
		t.Fatalf("error (%s) not nil\n", err)
	}

	if len(errorTasks) != 0 {
		t.Errorf("errorTasks (%d) != 0\n", len(errorTasks))
	}

	if len(requests) != 100 {
		t.Errorf("vertexTasks (%d) != 100\n", len(requests))
	}

	for i, request := range requests {
		if request.Payload != "new payload" {
			t.Errorf("payload (%s) != 'new payload'\n", request.Payload)
		}
		if request.ParentGraph != "graph" {
			t.Errorf("parent_graph (%s) != graph\n", request.ParentGraph)
		}
		if request.ParentVertex != "vertex" {
			t.Errorf("parent_vertex (%s) != vertex\n", request.ParentVertex)
		}
		if request.ParentID != tasks[i].ID {
			t.Errorf("parent_id (%s) != %s\n", request.ParentID, tasks[i].ID)
		}
		if request.Args.Priority != tasks[i].Args.Priority {
			t.Errorf("priority (%d) != %d\n", request.Args.Priority, tasks[i].Args.Priority)
		}
	}
}

func TestNewLuaMiddleware(t *testing.T) {
	t.Run("function not exist", testLuaMiddlewareFuntionNotExist)
	t.Run("syntax error", testLuaMiddlewareSyntaxError)
}

func TestLuaMiddleware_Filter(t *testing.T) {
	t.Run("lua error", testLuaMiddlewareFilterLuaError)
	t.Run("invalid return type", testLuaMiddlewareFilterInvalidReturnType)
	t.Run("filter all", testLuaMiddlewareFilterAll)
	t.Run("filter none", testLuaMiddlewareFilterNone)
}

func TestLuaMiddleware_Payload(t *testing.T) {
	t.Run("lua error", testLuaMiddlewarePayloadLuaError)
	t.Run("invalid return type", testLuaMiddlewarePayloadInvalidReturnType)
	t.Run("payload", testLuaMiddlewarePayload)
}
