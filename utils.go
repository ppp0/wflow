package main

func findGraph(graphs []Graph, name string) *Graph {
	for _, graph := range graphs {
		if graph.name == name {
			return &graph
		}
	}
	return nil
}

func findService(services []Service, name string) *Service {
	for _, service := range services {
		if service.name == name {
			return &service
		}
	}
	return nil
}

func findVertex(vertexes []*VertexService, name string) *VertexService {
	for _, vertex := range vertexes {
		if vertex.name == name {
			return vertex
		}
	}
	return nil
}
